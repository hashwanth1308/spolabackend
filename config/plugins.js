module.exports = ({ env }) => ({
    email: {
        provider: 'sendgrid',
        providerOptions: {
            apiKey: env('SENDGRID_API_KEY'),
        },
        settings: {
            defaultFrom: "building.materials@estateyards.in",
            defaultReplyTo: "building.materials@estateyards.in",
        },
    },

});