'use strict';
const { NodeMediaServer } = require('node-media-server');

const admin = require('firebase-admin');
// var serviceAccount = require("C:/Users/HASHWANTH/Downloads/spola-afc8d-firebase-adminsdk-hregm-02a4912d04.json");
/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/concepts/configurations.html#bootstrap
 */



module.exports = () => {
  const io = require("socket.io")(strapi.server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"],
    },
  });
  require('../../api/live/controllers/live')(io);

  // admin.initializeApp({
  //   credential: admin.credential.cert(serviceAccount),
  //   databaseURL: "https://spola-afc8d-default-rtdb.firebaseio.com"
  // });
  // strapi.firebase = admin;



  // const config = {
  //   rtmp: {
  //     port: 1935,
  //     chunk_size: 60000,
  //     gop_cache: true,
  //     ping: 30,
  //     ping_timeout: 60,
  //   },
  //   http: {
  //     port: 8000,
  //     allow_origin: '*',
  //   },
  // }
  // const nodeMediaServerConfig = {
  //   rtmp: {
  //     port: 1935,
  //     chunk_size: 60000,
  //     gop_cache: true,
  //     ping: 60,
  //     ping_timeout: 30,
  //   },
  //   http: {
  //     port: 8000,
  //     mediaroot: './media',
  //     allow_origin: '*',
  //   },
  //   trans: {
  //     ffmpeg: '/usr/local/bin/ffmpeg',
  //     tasks: [
  //       {
  //         app: 'live',
  //         ac: 'aac',
  //         mp4: true,
  //         mp4Flags: '[movflags=faststart]',
  //       },
  //     ],
  //   },
  // };

  const config = {
    rtmp: {
      port: 1935,
      chunk_size: 60000,
      gop_cache: true,
      ping: 30,
      ping_timeout: 60
    },
    http: {
      port: 8000,
      mediaroot: './media',
      allow_origin: '*'
    },
    trans: {
      ffmpeg: '/usr/bin/ffmpeg',
      tasks: [
        {
          app: 'live',
          vc: "copy",
          vcParam: [],
          ac: "aac",
          acParam: ['-ab', '64k', '-ac', '1', '-ar', '44100'],
          rtmp: true,
          rtmpApp: 'live2',
          hls: true,
          hlsFlags: '[hls_time=2:hls_list_size=3:hls_flags=delete_segments]',
          dash: true,
          dashFlags: '[f=dash:window_size=3:extra_window_size=5]',
          mp4: true,
          mp4Flags: '[movflags=frag_keyframe+empty_moov]',
        }
      ]
    }
  };

  const nms = new NodeMediaServer(config);
  nms.run();

};