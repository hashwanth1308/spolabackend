'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    find: ctx => {
        return strapi.query('channel-videos').find(ctx.query, [
            {
                path: 'channel',
                populate: {
                    path: 'channelCreatedBy',
                    populate:{
                        path:'followers',
                        populate:{
                            path:'followersProfiles'
                        }
                    }
                },
            },
            {
                path: 'channel',
                populate: {
                    path: 'subscriptions',
                    populate:{
                        path:'subscribedBy', 
                    }
                },
            },
            {
                path: 'uploadedVideo',
                populate: {
                    path: 'uploadedBy',
                   
                },
            },
            
        ]);
    },
};
