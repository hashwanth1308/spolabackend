'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('rewards').find(ctx.query, [
            {
                path: 'rewardedVideos',
                populate: {
                    path: 'uploadedBy',
                    
                },
            },
            {
                path: 'rewardedVideos',
                populate: {
                    path: 'channelVideo',
                    populate: {
                        path: 'channel',
                        
                    },
                    
                },
            },
        ]);
    },
};
