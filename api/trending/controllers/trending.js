'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('trending').find(ctx.query, [
            {
                path: 'videos',
                populate: {
                    path: 'uploadedBy',
                },
            },
            {
                path: 'videos',
                populate: {
                    path: 'channelVideo',
                    populate: {
                        path: 'channel',
                        populate: {
                            path: 'channelCreatedBy',
                        },
                    },
                },

            },
        ]);
    },
};
