'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('channel').find(ctx.query, [
            
            {
                path: 'channelVideo',
                populate: {
                    path: 'uploadedVideo',
                    populate: {
                        path: 'uploadedBy',
                    },
                },
            },
            {
                path: 'subscriptions',
                populate: {
                    path: 'subscribedBy',
                    
                },
            },
            {
                path: 'channelCreatedBy',
                populate: {
                    path: 'channels',
                   
                },
            },
        ]);
    }
};
