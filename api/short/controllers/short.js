'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('short').find(ctx.query, [
            {
                path: 'uploadedBy',
                populate: {
                    path: 'followers',
                    populate: {
                        path: 'followersProfiles',
                    },
                },
            },
            {
                path: 'uploadedBy',
                populate: {
                    path: 'shorts',
                   
                },
            },
            {
                path: 'uploadedBy',
                populate: {
                    path: 'videos',
                    
                   
                },
            },
            {
                path: 'comments',
                populate: {
                    path: 'commentedBy',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles',
                        },
                    },
                },
            },
            {
                path: 'comments',
                populate: {
                    path: 'commentedBy',
                    populate: {
                        path: 'shorts',
                       
                    },
                },
            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles',
                        },
                    },
                },
            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                    populate: {
                        path: 'shorts',
                    }
                    },
                },
            {
                path: 'dislike',
                populate: {
                    path: 'dislikedBy',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles',
                        },
                    },
                },
            },
            {
                path: 'dislike',
                populate: {
                    path: 'disLikedBy',
                    populate: {
                        path: 'shorts',
                        
                    },
                },
            },
           
        ])
    }
};
