const slugify = require['slugify'];

module.exports = {
    beforesave: async (model, attrs, options) => {
        if (options.method === 'insert' && attrs.title) {
            model.set('type', slugify(attrs.title));

        }
        else if (options.method === 'update' && attrs.title) {
            attrs.type = slugify(attrs.title);
        }

    },
};


