var ID = require("nodejs-unique-numeric-id-generator")
const admin = require('firebase-admin');
console.log(ID.generate(new Date().toJSON()));
module.exports = {
    async create(ctx) {
        console.log(ctx.request.body);
        ctx.request.body.productId = ID.generate(new Date().toJSON())

        const message = {
            data: {
                type: 'warning',
                content: 'A new weather warning has been created!',
            },
            topic: 'weather',
        };

        admin
            .messaging()
            .send(message)
            .then(response => {
                console.log('Successfully sent message:', response);
            })
            .catch(error => {
                console.log('Error sending message:', error);
            });
    }
};