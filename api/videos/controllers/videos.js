'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    // findOne: ctx => {
    //     console.log(ctx);
    //   return strapi.query('videos').findOne(ctx.request.url, ['uploadedBy', 'uploadedBy.videos'])
    // },
    findOne: ctx => {
        console.log(ctx);
        const { id } = ctx.params;
        return strapi.query('videos').findOne({ id },[
            {
                path: 'channelVideo',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: {
                    path: 'channel',
                }
            },
            {
                path: 'uploadedBy',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: {
                    path: 'videos',
                },
            },
            {
                path: 'comment',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: {
                    path: 'commentedBy',
                },
            },
            {
                path: 'likes',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: {
                    path: 'likedBy',
                },
            },
            {
                path: 'dislikes',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: {
                    path: 'disLikedBy',
                },
            },
        ])
    },

    find: ctx => {
        return strapi.query('videos').find(ctx.query, [
            {
                path: 'channelVideo',
                populate: {
                    path: 'channel',
                    populate: {
                        path: 'channelCreatedBy',

                    },
                },

            },
            {
                path: 'channelVideo',
                populate: {
                    path: 'channel',
                    populate: {
                        path: 'subscriptions',

                    },
                },

            },
            {
                path: 'channelVideo',
                populate: {
                    path: 'uploadedVideo',
                    populate: {
                        path: 'uploadedBy',

                    },

                },

            },
            {
                path: 'bookMarkVideo',
                populate: {
                    path: 'bookmarkedBy',
                },

            },
            {
                path: 'bookMarkVideo',
                populate: {
                    path: 'savedVideo',
                },

            },
            {
                path: 'dislikes',
                populate: {
                    path: 'disLikedBy',

                },

            },
            {
                path: 'history',
                populate: {
                    path: 'video',
                },

            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                    populate: {
                        path: 'videos',

                    },

                },
            },
            {
                path: 'uploadedBy',
                populate: {
                    path: 'videos',
                    populate: {
                        path: 'uploadedBy',
                    },
                },

            },
            {
                path: 'uploadedBy',
                populate: {
                    path: 'shorts',
                    populate: {
                        path: 'uploadedBy',
                    },
                },

            },
            {
                path: 'comment',
                populate: {
                    path: 'commentedBy',
                    populate: {
                        path: 'videos',
                    },

                },
            },
            {
                path: 'comment',
                populate: {
                    path: 'commentVideo',
                    populate: {
                        path: 'uploadedBy',
                    },

                },
            },
            {
                path: 'uploadedBy',
                populate: {
                    path: 'videos',
                },

            },



        ]);
    },
};





