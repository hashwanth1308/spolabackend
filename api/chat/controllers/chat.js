'use strict';


/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    find: async ctx => {
        return strapi.query('chat').model.find({ $or: [{ $and: [{ "messageFromUser": ctx.query.messageFromUser }, { "messageToUser": ctx.query.messageToUser }] }, { $and: [{ "messageFromUser": ctx.query.messageToUser }, { "messageToUser": ctx.query.messageFromUser }] }] })
    },
    find: ctx => {
        return strapi.query('chat').find(ctx.query, [
            {
                path: 'messageFromUser',
                populate: {
                    path: 'videos',
                },
            },
            {
                path: 'messageToUser',
                populate: {
                    path: 'videos',
                },
            },
        ]);
    },

    // async create(ctx) {
    //     let entity;
    //     if (ctx.is("multipart")) {
    //         const { data, files } = parseMultipartData(ctx);
    //         entity = await strapi.services.chat.create(data, { files });
    //     } else {
    //         entity = await strapi.services.chat.create(ctx.request.body);
    //     }
    //     strapi.StrapIO.emit(this, ctx, 'create', entity, 'chat');

    //     return sanitizeEntity(entity, { model: strapi.models.chat });
    // },

    // async update(ctx) {
    //     const { id } = ctx.params;

    //     let entity;
    //     if (ctx.is("multipart")) {
    //         const { data, files } = parseMultipartData(ctx);
    //         entity = await strapi.services.chat.update({ id }, data, {
    //             files,
    //         });
    //     } else {
    //         entity = await strapi.services.chat.update({ id }, ctx.request.body);
    //     }
    //     strapi.StrapIO.emit(this, ctx, 'update', entity, 'chat');

    //     return sanitizeEntity(entity, { model: strapi.models.chat });
    // }

};
