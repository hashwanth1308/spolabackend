'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('recommendation').find(ctx.query, [
            {
                path: 'upload_videos',
                populate: {
                    path: 'uploadedBy',
                },
            },
            {
                path: 'upload_videos',
                populate: {
                    path: 'channelVideo',
                    populate: {
                        path: 'channel',
                },
            },
        },
        ]);
    },
};
