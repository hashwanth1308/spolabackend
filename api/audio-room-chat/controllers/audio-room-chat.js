'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('audio-room-chat').find(ctx.query, [
            {
                path: 'audio_rooms_id',
                populate: {
                    path: 'audioRoomPeople',
                },
            },
            {
                path: 'audio_rooms_id',
                populate: {
                    path: 'audioRoomAdmins',
                },
            },
           
            
        ]);
    },
};
