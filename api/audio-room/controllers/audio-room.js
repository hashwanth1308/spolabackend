'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('audio-room').find(ctx.query, [
            {
                path: 'audioRoomPeople',
                populate: {
                    path: 'profileFollowings',
                    populate: {
                        path: 'profileFollowings',
                    },
                },
            },
            {
                path: 'audioRoomPeople',
                populate: {
                    path: 'followers',
                    populate: {
                        path: 'followersProfiles',
                    },
                },
            },
            {
                path: 'audioRoomPeople',
                populate: {
                    path: 'videos',
                    populate: {
                        path: 'uploadedBy',
                        
                    },
                },
            },
            {
                path: 'audioRoomPeople',
                populate: {
                    path: 'feed',
                    populate: {
                        path: 'feedPostedBy',
                        
                    },
                },
            },
            {
                path: 'audioRoomPeople',
                populate: {
                    path: 'shorts',
                    populate: {
                        path: 'uploadedBy',
                        
                    },
                },
            },
                {
                    path: 'audioRoomAdmins',
                    populate: {
                        path: 'profileFollowings',
                        populate: {
                            path: 'profileFollowings',
                            
                        },
                        
                    },
                },
                {
                    path: 'audioRoomAdmins',
                    populate: {
                        path: 'videos',
                        populate: {
                            path: 'uploadedBy',
                            
                        },
                        
                    },
                },
                {
                    path: 'audioRoomAdmins',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles',
                           
                        },
                       
                    },
            },
            {
                path: 'audioRoomAdmins',
                populate: {
                    path: 'feed',
                    populate: {
                        path: 'feedPostedBy',
                        
                    },
                    
                },
            },
            {
                path: 'audioRoomAdmins',
                populate: {
                    path: 'shorts',
                    populate: {
                        path: 'uploadedBy',
                        
                    },
                    
                },
            },
            
        ]);
    },
};
