'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('comments').find(ctx.query, [
            {
                path: 'commentVideo',
                populate: {
                    path: 'uploadedBy',
                },
            },
            {
                path: 'commentFeed',
                populate: {
                    path: 'feedPostedBy',
                },
            },
            {
                path: 'commentedBy',
                populate: {
                    path: 'comments',
                },
            },
           
            
        ]);
    },
    findOne: ctx => {
        return strapi.query('comments').findOne(ctx.query, [
            {
                path: 'commentVideo',
                populate: {
                    path: 'uploadedBy',
                },
            },
            {
                path: 'commentFeed',
                populate: {
                    path: 'feedPostedBy',
                },
            },
            {
                path: 'commentedBy',
                populate: {
                    path: 'comments',
                },
            },
           
           
            
        ]);
    },
};
    

