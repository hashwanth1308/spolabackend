'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('history').find(ctx.query, [
            {
                path: 'video',
                populate: {
                    path: 'uploadedBy',
                },
            },
            {
                path: 'video',
                populate: {
                    path: 'channelVideo',
                    populate: {
                        path: 'channel',
                    },
                },
                },
                  
        ]);
    },
};
