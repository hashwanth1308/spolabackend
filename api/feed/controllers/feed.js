'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    find: ctx => {
        return strapi.query('feed').find(ctx.query, [
            {
                path: 'comments',
                populate: {
                    path: 'commentedBy',
                    populate: {
                        path: 'profileFollowings',
                        populate: {
                            path: 'profileFollowings',
                        },
                    },
                },
            },
            {
                path: 'comments',
                populate: {
                    path: 'commentedBy',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles',
                        },
                    },
                },
            },
            {
                path: 'comments',
                populate: {
                    path: 'commentedBy',
                    populate: {
                        path: 'shorts',
                    },
                },
            },
            {
                path: 'comments',
                populate: {
                    path: 'commentedBy',
                    populate: {
                        path: 'feed',
                    },
                },
            },
            {
                path: 'comments',
                populate: {
                    path: 'commentedBy',
                },
            },
            {
                path: 'feedPostedBy',
                populate: {
                    path: 'profileFollowings',
                    populate: {
                        path: 'profileFollowings',
                    },
                },
            },
            {
                path: 'feedPostedBy',
                populate: {
                    path: 'followers',
                    populate: {
                        path: 'followersProfiles',
                    },
                },
            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                    populate: {
                        path: 'profileFollowings',
                        populate: {
                            path: 'profileFollowings',
                        },
                    },
                },
            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles',
                        },
                    },
                },
            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                    populate: {
                        path: 'shorts',
                    },
                },
            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                    populate: {
                        path: 'feed',
                    },
                },
            },
            {
                path: 'likes',
                populate: {
                    path: 'likedBy',
                },
            },
            {
                path: 'dislikes',
                populate: {
                    path: 'disLikedBy',
                },
            },
            {
                path: 'bookmarks',
                populate: {
                    path: 'bookmarkedBy',
                    populate: {
                        path: 'profileFollowings',
                        populate: {
                            path: 'profileFollowings',
                    },
                },
            },
        },
        {
            path: 'bookmarks',
            populate: {
                path: 'bookmarkedBy',
                populate: {
                    path: 'followers',
                    populate: {
                        path: 'followersProfiles',
                },
            },
        },
    },
        ]);
    },
};
