# Spola Application:

Spola application is a video streaming application, Where we can upload videos,images,shorts.  


# Getting Started:

# Installation:

Make sure Node.js, npm and MongoDB are properly installed on your machine. 

Install Strapi  to create a Spola project instantly:

Use npm/npx to install the Spola project:

Once the installation is complete, your browser automatically opens 2 tabs:

The first tab (http://localhost:1337/admin/auth/register-admin (opens new window)) is the admin panel, it's 

for the back end of your application.

The second tab (http://localhost:1337 (opens new window)) is for the front end of your application.

By completing the form in the first tab, you create your own account. Once done, you become the first 

administator user of this Spola application.

# Getting Started with Spola Application:

Go to the repository and fork it to your own GitHub account.

Fork the repository and create your branch from master.

Clone the project.

"git clone http:git@github.com:YOUR_USERNAME/strapi.gi"

 Go to the root of the project, Open the terminal and run the following command  

"cd Spola"

Then, install node modulues with the following command in project directory

"npm install"

Once the installation is complete, build the admin panel with the command

"npm run build"

 Running the administration panel in development mode, Start the administration panel server for development

"npm run develop"

The administration panel will be available at http://localhost:1337/admin.

# Repository Organisation:

All the work done is available on Git. 


# Requirements:


# Node:

 NodeJS ">=10.16.0 <=14.x.x"

 NPM "^6.0.0"

 # Database:

 MongoDB ">=4.4.0"

 





