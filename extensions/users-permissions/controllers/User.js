
const { users } = require("../models/User.settings.json");

module.exports = {
    async find(ctx) {
        return result1 = await strapi.query('user', 'users-permissions').model.find(ctx.query)
            .populate({
               
                
                path: 'shorts',
                populate: {
                    path: 'uploadedBy',
                    populate: {
                        path: 'videos'
                    }
                },
                path: 'shorts',
                populate: {
                    path: 'uploadedBy',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles'
                        }
                    }
                },
                path: 'shorts',
                populate: {
                    path: 'uploadedBy',
                },
                path: 'shorts',
                populate: {
                    path: 'comments',
                    populate: {
                        path: 'commentedBy'
                    }
                },
                path: 'playList',
                populate: {
                    path: 'playlistVideo',
                    populate: {
                        path: 'video'
                    }
                },
                path: 'playList',
                populate: {
                    path: 'playlistVideo',
                    populate: {
                        path: 'video',
                        populate: {
                            path: 'channelVideo',
                            populate: {
                                path: 'channel'
                            }
                        }
                    }
                },
                path: 'feed',
                populate: {
                    path: 'comments',
                    populate: {
                        path: 'commentedBy',
                        populate: {
                            path: 'followers',
                            populate: {
                                path: 'followersProfiles'
                            }
                        }
                    }
                },
                path: 'feed',
                populate: {
                    path: 'comments',
                    populate: {
                        path: 'commentedBy',
                    }
                },
                path: 'feed',
                populate: {
                    path: 'likes',
                    populate: {
                        path: 'likedBy'
                    }
                },
                path: 'feed',
                populate: {
                    path: 'feedPostedBy',
                    populate: {
                        path: 'followers',
                        populate: {
                            path: 'followersProfiles',
                           
                        },
                       
                    },
                   
                },
                path: 'feed',
                populate: {
                    path: 'feedPostedBy',
                   
                },
                path: 'histories',
                populate: {
                    path: 'video',
                    populate: {
                        path: 'uploadedBy'
                    }
                },
                path: 'histories',
                populate: {
                    path: 'video',
                    populate: {
                        path: 'channelVideo',
                        populate: {
                            path: 'channel'
                        }
                    }
                },
            
            path: 'subscriptions',
            populate: {
                path: 'subscribedBy',
                
            },
        
        path: 'subscriptions',
        populate: {
            path: 'channel',
            populate: {
                path: 'channel_video'
            }
        },
         
        path: 'channels',
        populate: {
            path: 'channel_video',
            
        },
        path: 'videos',
        populate: {
            path: 'uploadedBy',
            
        },
        path: 'videos',
        populate: {
            path: 'channelVideo',
            populate: {
                path: 'channel'
            }
        },

                  path: 'shorts',
                populate: {
                    path: 'bgms',
                    populate: {
                        path: 'bgmUploadedBy'
                    }
                },

                 path: 'shorts',
                 populate: {
                    path: 'likes',
                    populate: {
                         path: 'likedBy'
                    }
                 
                },
                
                path: 'shorts',
                populate: {
                   path: 'dislike',
                   populate: {
                        path: 'disLikedBy'
                   }
                
               },
               


            })
    }
}